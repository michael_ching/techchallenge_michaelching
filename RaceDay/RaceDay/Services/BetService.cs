﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using RaceDay.Adapters;
using RaceDay.Models;

namespace RaceDay.Services
{
    public interface IBetService
    {
        Task<IEnumerable<Bet>> GetBets();
    }

    public class BetService : IBetService
    {
        private readonly IBetApiAdapter _betApiAdapter;

        public BetService(IBetApiAdapter betApiAdapter)
        {
            _betApiAdapter = betApiAdapter;
        }

        public async Task<IEnumerable<Bet>> GetBets()
        {
            var bets = await _betApiAdapter.Get();

            return bets.Select(b => new Bet
            {
                CustomerId = b.CustomerId,
                HorseId = b.HorseId,
                RaceId = b.RaceId,
                Stake = b.Stake
            });
        }
    }
}