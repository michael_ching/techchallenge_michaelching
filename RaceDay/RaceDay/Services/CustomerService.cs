﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RaceDay.Adapters;
using RaceDay.Models;

namespace RaceDay.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> GetCustomers();
        Task<CustomerDetails> GetCustomerDetails(int customerId);
        Task<IEnumerable<Customer>> GetAtRiskCustomers();
        Task<double> GetTotalBetAmount();
    }

    public class CustomerService : ICustomerService
    {
        private readonly ICustomerApiAdapter _customerApiAdapter;
        private readonly IBetApiAdapter _betApiAdapter;

        private static readonly int _atRiskAmount = 200;

        public CustomerService(ICustomerApiAdapter customerApiAdapter, IBetApiAdapter betApiAdapter)
        {
            _customerApiAdapter = customerApiAdapter;
            _betApiAdapter = betApiAdapter;
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            var customers = await _customerApiAdapter.Get();
            return customers.Select(c => new Customer
            {
                Id = c.Id,
                Name = c.Name
            });
        }

        public async Task<CustomerDetails> GetCustomerDetails(int customerId)
        {
            var customers = await _customerApiAdapter.Get();
            var customer = customers.FirstOrDefault(c => c.Id == customerId);

            if (customer == null)
                return null;

            var bets = await _betApiAdapter.Get();
            return new CustomerDetails
            {
                Id = customerId,
                Name = customer.Name,
                NumOfBets = bets.Count(b => b.CustomerId == customerId),
                TotalBetAmount = bets.Where(b => b.CustomerId == customerId).Sum(b => b.Stake)
            };
        }

        public async Task<IEnumerable<Customer>> GetAtRiskCustomers()
        {
            var customers = await _customerApiAdapter.Get();
            var bets = await _betApiAdapter.Get();

            var customerBetGroups = bets.GroupBy(
                b => b.CustomerId,
                b => b.Stake,
                (customerId, totalAmount) =>
                    new
                    {
                        Customer = customers.First(c => c.Id == customerId),
                        TotalAmount = totalAmount.Sum()
                    });

            return customerBetGroups
                .Where(cb => cb.TotalAmount >= _atRiskAmount)
                .Select(c => new Customer
                {
                    Id = c.Customer.Id,
                    Name = c.Customer.Name
                });
        }

        public async Task<double> GetTotalBetAmount()
        {
            var bets = await _betApiAdapter.Get();
            
            return bets.Sum(b => b.Stake);
        }
    }
}