﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using RaceDay.Adapters;
using RaceDay.Models;

namespace RaceDay.Services
{
    public interface IRaceService
    {
        Task<IEnumerable<Race>> GetRaces();
    }

    public class RaceService : IRaceService
    {
        private readonly IRaceApiAdapter _raceApiAdapter;
        private readonly IBetApiAdapter _betApiAdapter;

        public RaceService(IRaceApiAdapter raceApiAdapter, IBetApiAdapter betApiAdapter)
        {
            _raceApiAdapter = raceApiAdapter;
            _betApiAdapter = betApiAdapter;
        }

        public async Task<IEnumerable<Race>> GetRaces()
        {
            var races = await _raceApiAdapter.Get();
            var bets = (await _betApiAdapter.Get()).ToList();

            return races.Select(r => new Race
            {
                Amount = bets.Where(b => b.RaceId == r.Id).Sum(b => b.Stake),
                Id = r.Id,
                Name = r.Name,
                StartTime = r.Start,
                Status = (Status)Enum.Parse(typeof(Status), r.Status.ToString()),
                Horses = r.Horses.Select(h => new Horse
                {
                    Id = h.Id,
                    Name = h.Name,
                    Odds = h.Odds,
                    NumOfBets = bets.Count(b => b.HorseId == h.Id),
                    Payout = CalculatePayoutOnHorse(bets.AsEnumerable(), h)
                })
            });
        }

        private double CalculatePayoutOnHorse(IEnumerable<Proxies.Bet> bets, Proxies.Horse horse)
        {
            return bets.Where(b => b.HorseId == horse.Id).Sum(b => b.Stake) * horse.Odds;
        }
    }
}