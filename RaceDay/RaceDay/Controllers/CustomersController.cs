﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RaceDay.Services;
using System.Linq;

namespace RaceDay.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var customers = await _customerService.GetCustomers();

            return Ok(customers);
        }

        [HttpGet("AtRisk")]
        public async Task<IActionResult> GetAtRiskCustomers()
        {
            var atRiskCustomers = await _customerService.GetAtRiskCustomers();

            return Ok(atRiskCustomers);
        }

        [HttpGet("{customerId}")]
        public async Task<IActionResult> GetCustomer(int customerId)
        {
            var customer = await _customerService.GetCustomerDetails(customerId);

            if (customer == null)
                return NoContent();

            return Ok(customer);
        }

        [HttpGet("TotalBetAmount")]
        public async Task<IActionResult> GetTotalBetAmount()
        {
            var amount = await _customerService.GetTotalBetAmount();
            return Ok(amount);
        }
    }
}