﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RaceDay.Services;
using System.Linq;

namespace RaceDay.Controllers
{
    [Route("api/[controller]")]
    public class RacesController : Controller
    {
        private readonly IRaceService _raceService;

        public RacesController(IRaceService raceService)
        {
            _raceService = raceService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var races = await _raceService.GetRaces();

            return Ok(races);
        }
    }
}