﻿using System;
using System.Collections.Generic;

namespace RaceDay.Proxies
{
    public class Race
    {
        public Status Status { get; set;}
        public DateTime Start { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public IEnumerable<Horse> Horses { get; set; }
    }
}
