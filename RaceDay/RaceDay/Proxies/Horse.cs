﻿namespace RaceDay.Proxies
{
    public class Horse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Odds { get; set; }
    }
}