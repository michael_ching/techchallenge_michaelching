﻿namespace RaceDay.Proxies
{
    public enum Status
    {
        Pending,
        InProgress,
        Completed
    }
}