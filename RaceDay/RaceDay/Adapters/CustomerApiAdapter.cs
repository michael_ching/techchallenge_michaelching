﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RaceDay.Adapters
{
    public interface ICustomerApiAdapter
    {
        Task<IEnumerable<Proxies.Customer>> Get();
    }

    public class CustomerApiAdapter : ICustomerApiAdapter
    {
        private readonly IHttpClientWrapper _client;
        private static readonly string _getCustomersUrl = "https://whatech-customerbets.azurewebsites.net/api/GetCustomers?name=michaelChing";

        public CustomerApiAdapter(IHttpClientWrapper client)
        {
            _client = client;
        }

        public async Task<IEnumerable<Proxies.Customer>> Get()
        {
            var stringResult = await _client.GetAsStringAsync(_getCustomersUrl);
            var customers = JsonConvert.DeserializeObject<IEnumerable<Proxies.Customer>>(stringResult);

            return customers;
        }
    }
}