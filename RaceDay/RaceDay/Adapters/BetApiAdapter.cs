﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RaceDay.Adapters
{
    public interface IBetApiAdapter
    {
        Task<IEnumerable<Proxies.Bet>> Get();
    }

    public class BetApiAdapter : IBetApiAdapter
    {
        private readonly IHttpClientWrapper _client;
        private static readonly string _getBetsUrl = "https://whatech-customerbets.azurewebsites.net/api/GetBetsV2?name=michaelChing";

        public BetApiAdapter(IHttpClientWrapper client)
        {
            _client = client;
        }

        public async Task<IEnumerable<Proxies.Bet>> Get()
        {
            var stringResult = await _client.GetAsStringAsync(_getBetsUrl);
            var bets = JsonConvert.DeserializeObject<IEnumerable<Proxies.Bet>>(stringResult);

            return bets;
        }
    }
}