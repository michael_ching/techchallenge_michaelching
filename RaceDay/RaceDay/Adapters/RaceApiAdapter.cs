﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RaceDay.Adapters
{
    public interface IRaceApiAdapter
    {
        Task<IEnumerable<Proxies.Race>> Get();
    }

    public class RaceApiAdapter : IRaceApiAdapter
    {
        private readonly IHttpClientWrapper _client;
        private static readonly string _getRacesUrl = "https://whatech-customerbets.azurewebsites.net/api/GetRaces?name=michaelChing";

        public RaceApiAdapter(IHttpClientWrapper client)
        {
            _client = client;
        }

        public async Task<IEnumerable<Proxies.Race>> Get()
        {
            var stringResult = await _client.GetAsStringAsync(_getRacesUrl);
            var races = JsonConvert.DeserializeObject<IEnumerable<Proxies.Race>>(stringResult);

            return races;
        }
    }
}