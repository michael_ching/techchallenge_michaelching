﻿using System.Net.Http;
using System.Threading.Tasks;

namespace RaceDay.Adapters
{
    public interface IHttpClientWrapper
    {
        Task<string> GetAsStringAsync(string url);
    }

    public class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly HttpClient _httpClient;

        public HttpClientWrapper()
        {
            _httpClient = new HttpClient();
        }

        public async Task<string> GetAsStringAsync(string url)
        {
            var response = await _httpClient.GetAsync(url);
            response.EnsureSuccessStatusCode();

            var stringResult = await response.Content.ReadAsStringAsync();
            return stringResult;
        }
    }
}