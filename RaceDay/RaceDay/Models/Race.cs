﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace RaceDay.Models
{
    public class Race
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }
        public double Amount { get; set; }
        public DateTime StartTime { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public IEnumerable<Horse> Horses { get; set; }
    }
}