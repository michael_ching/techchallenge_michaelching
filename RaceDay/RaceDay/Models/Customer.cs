﻿namespace RaceDay.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CustomerDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumOfBets { get; set; }
        public double TotalBetAmount { get; set; }
    }
}