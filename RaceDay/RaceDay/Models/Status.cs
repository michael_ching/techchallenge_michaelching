﻿namespace RaceDay.Models
{
    public enum Status
    {
        Pending,
        InProgress,
        Completed
    }
}