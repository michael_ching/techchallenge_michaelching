﻿namespace RaceDay.Models
{
    public class Horse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Odds { get; set; }
        public int NumOfBets { get; set; }
        public double Payout { get; set; }
    }
}