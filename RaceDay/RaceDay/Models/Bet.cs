﻿namespace RaceDay.Models
{
    public class Bet
    {
        public int HorseId { get; set; }
        public int CustomerId { get; set; }
        public int RaceId { get; set; }
        public double Stake { get; set; }
    }
}