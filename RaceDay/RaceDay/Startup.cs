﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RaceDay.Adapters;
using RaceDay.Controllers;
using RaceDay.Services;

namespace RaceDay
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IRaceService, RaceService>();
            services.AddTransient<IBetService, BetService>();
            services.AddTransient<ICustomerService, CustomerService>();

            services.AddTransient<IRaceApiAdapter, RaceApiAdapter>();
            services.AddTransient<IBetApiAdapter, BetApiAdapter>();
            services.AddTransient<ICustomerApiAdapter, CustomerApiAdapter>();

            services.AddTransient<IHttpClientWrapper, HttpClientWrapper>();
            services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin()
                                                                    .AllowAnyMethod()
                                                                     .AllowAnyHeader()));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAll");
            app.UseMvc();
        }
    }
}
