﻿using NUnit.Framework;
using RaceDay.Adapters;
using System.Linq;
using System.Threading.Tasks;

namespace RaceDay.Tests
{
    [TestFixture]
    public class RaceDayIntegrationTests
    {
        private IHttpClientWrapper _httpClientWrapper;

        [SetUp]
        public void SetUp()
        {
            _httpClientWrapper = new HttpClientWrapper();
        }

        [Test]
        public async Task RaceApi_Should_Return_Results()
        {
            var adapter = new RaceApiAdapter(_httpClientWrapper);
            var result = await adapter.Get();
            Assert.IsTrue(result.Any());
        }

        [Test]
        public async Task BetApi_Should_Return_Results()
        {
            var adapter = new BetApiAdapter(_httpClientWrapper);
            var result = await adapter.Get();
            Assert.IsTrue(result.Any());
        }

        [Test]
        public async Task CustomerApi_Should_Return_Results()
        {
            var adapter = new CustomerApiAdapter(_httpClientWrapper);
            var result = await adapter.Get();
            Assert.IsTrue(result.Any());
        }
    }
}
