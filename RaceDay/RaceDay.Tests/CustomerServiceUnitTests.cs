﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using RaceDay.Adapters;
using RaceDay.Controllers;
using RaceDay.Models;
using RaceDay.Services;

namespace RaceDay.Tests
{
    [TestFixture()]
    public class CustomerServiceUnitTests
    {
        [Test]
        public async Task GetCustomerDetails_Should_Return_Customer_Bets()
        {
            var betApi = Substitute.For<IBetApiAdapter>();
            var customerApi = Substitute.For<ICustomerApiAdapter>();

            customerApi.Get().Returns(new List<Proxies.Customer>()
            {
                new Proxies.Customer() {Id = 1, Name = "Name"}
            });

            betApi.Get().Returns(new List<Proxies.Bet>()
            {
                new Proxies.Bet() {CustomerId = 1, Stake = 10},
                new Proxies.Bet() {CustomerId = 2, Stake = 10},
                new Proxies.Bet() {CustomerId = 1, Stake = 20},
            });

            var customerDetails = await new CustomerService(customerApi, betApi).GetCustomerDetails(1);
            
            Assert.AreEqual(2, customerDetails.NumOfBets);
            Assert.AreEqual(30, customerDetails.TotalBetAmount);
        }

        [Test]
        public async Task GetAtRiskCustomers_Should_Return_CustomersAboveLimit()
        {
            var betApi = Substitute.For<IBetApiAdapter>();
            var customerApi = Substitute.For<ICustomerApiAdapter>();

            customerApi.Get().Returns(new List<Proxies.Customer>()
            {
                new Proxies.Customer() {Id = 1, Name = "Name"},
                new Proxies.Customer() {Id = 2, Name = "Name 2"}
            });

            betApi.Get().Returns(new List<Proxies.Bet>()
            {
                new Proxies.Bet() {CustomerId = 1, Stake = 100},
                new Proxies.Bet() {CustomerId = 2, Stake = 300},
                new Proxies.Bet() {CustomerId = 1, Stake = 20},
            });

            var atRiskCustomers = await new CustomerService(customerApi, betApi).GetAtRiskCustomers();

            Assert.IsTrue(atRiskCustomers.Count() == 1);
            Assert.IsTrue(atRiskCustomers.First().Id == 2);
        }

        [Test]
        public void CustomersController_ShouldReturnNoContent_WhenNoMatchingCustomerIsFound()
        {
            var service = Substitute.For<ICustomerService>();
            service.GetCustomerDetails(Arg.Any<int>()).Returns((CustomerDetails)null);

            var customersController = new CustomersController(service);
            var response = customersController.GetCustomer(1);

            Assert.IsInstanceOf<NoContentResult>(response.Result);
        }

        [Test]
        public void CustomersController_ShouldReturnOk_WhenCustomerIsFound()
        {
            var service = Substitute.For<ICustomerService>();
            service.GetCustomerDetails(Arg.Any<int>()).Returns(Task.FromResult(new CustomerDetails()));

            var customersController = new CustomersController(service);
            var response = customersController.GetCustomer(1);

            Assert.IsInstanceOf<OkObjectResult>(response.Result);
        }

    }
}