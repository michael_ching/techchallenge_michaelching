﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using RaceDay.Adapters;
using RaceDay.Proxies;
using RaceDay.Services;
using RaceDay.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace RaceDay.Tests
{
    [TestFixture]
    public class RaceServiceUnitTests
    {
        [Test]
        public async Task GetRaces_Should_Aggregate_Bet_Amounts()
        {
            var raceApi = Substitute.For<IRaceApiAdapter>();
            var betApi = Substitute.For<IBetApiAdapter>();

            betApi.Get().Returns(new List<Bet>()
            {
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 2, Stake = 10},
                new Bet() {RaceId = 2, Stake = 10},
            });

            raceApi.Get().Returns(new List<Race>()
            {
                new Race() {Id = 1, Name = "Race 1", Horses = new List<Horse> (){ new Horse{Id = 1, Odds = 10}, new Horse{Id = 2, Odds = 10} }},
                new Race() {Id = 2, Name = "Race 2", Horses = new List<Horse> (){ new Horse{Id = 3, Odds = 10}, new Horse{Id = 4, Odds = 10} }}
            });

            var sut = new RaceService(raceApi, betApi);
            var races = await sut.GetRaces();

            Assert.AreEqual(40, races.Single(r => r.Id == 1).Amount);
        }

        [Test]
        public async Task GetRaces_Should_Aggregate_Bet_Amounts_On_Horses()
        {
            var raceApi = Substitute.For<IRaceApiAdapter>();
            var betApi = Substitute.For<IBetApiAdapter>();

            betApi.Get().Returns(new List<Bet>()
            {
                new Bet() {HorseId = 1, Stake = 10},
                new Bet() {HorseId = 1, Stake = 10},
                new Bet() {HorseId = 1, Stake = 10},
                new Bet() {HorseId = 1, Stake = 10},
                new Bet() {HorseId = 2, Stake = 10},
                new Bet() {HorseId = 2, Stake = 10},
            });

            raceApi.Get().Returns(new List<Race>()
            {
                new Race() {Id = 1, Name = "Race 1", Horses = new List<Horse> (){ new Horse{Id = 1, Odds = 10}, new Horse{Id = 2, Odds = 10} }},
                new Race() {Id = 2, Name = "Race 2", Horses = new List<Horse> (){ new Horse{Id = 3, Odds = 10}, new Horse{Id = 4, Odds = 10} }}
            });

            var sut = new RaceService(raceApi, betApi);
            var races = await sut.GetRaces();

            Assert.AreEqual(400, races.Single(r => r.Id == 1).Horses.Single(h => h.Id == 1).Payout);
        }

        [Test]
        public async Task GetRaces_Amounts_Should_Be_0_If_There_Are_No_Bets()
        {
            var raceApi = Substitute.For<IRaceApiAdapter>();
            var betApi = Substitute.For<IBetApiAdapter>();

            betApi.Get().Returns(new List<Bet>()
            {
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 1, Stake = 10},
                new Bet() {RaceId = 1, Stake = 10},
            });

            raceApi.Get().Returns(new List<Race>()
            {
                new Race() {Id = 1, Name = "Race 1", Horses = new List<Horse> (){ new Horse{Id = 1}, new Horse{Id = 2} } },
                new Race() {Id = 2, Name = "Race 2", Horses = new List<Horse> (){ new Horse{Id = 3}, new Horse{Id = 4} } }
            });

            var sut = new RaceService(raceApi, betApi);
            var races = await sut.GetRaces();

            Assert.AreEqual(0, races.Single(r => r.Id == 2).Amount);
        }

        [Test]
        public async Task GetRaces_Mapping_Test()
        {
            var raceApi = Substitute.For<IRaceApiAdapter>();
            var betApi = Substitute.For<IBetApiAdapter>();

            var raceTime = DateTime.Now;

            betApi.Get().Returns(new List<Bet>()
            {
                new Bet() {RaceId = 1, CustomerId = 2, HorseId = 3, Stake = 10},
            });

            raceApi.Get().Returns(new List<Race>()
            {
                new Race() {Id = 1, Name = "Race 1", Start = raceTime, Status = Status.InProgress, Horses = new List<Horse> { new Horse { Id = 1, Name = "Horse 1", Odds = 2 } } },
            });

            var sut = new RaceService(raceApi, betApi);
            var races = await sut.GetRaces();

            var race = races.Single(r => r.Id == 1);
            Assert.AreEqual(10, race.Amount);
            Assert.AreEqual(1, race.Id);
            Assert.AreEqual("Race 1", race.Name);
            Assert.AreEqual(raceTime, race.StartTime);
            Assert.AreEqual(Models.Status.InProgress, race.Status);
        }

        public async Task GetRaces_Horses_Mapping_Test()
        {
            var raceApi = Substitute.For<IRaceApiAdapter>();
            var betApi = Substitute.For<IBetApiAdapter>();

            var raceTime = DateTime.Now;

            betApi.Get().Returns(new List<Bet>()
            {
                new Bet() {RaceId = 1, CustomerId = 2, HorseId = 3, Stake = 10},
            });

            raceApi.Get().Returns(new List<Race>()
            {
                new Race() {Id = 1, Name = "Race 1", Start = raceTime, Status = Status.InProgress, Horses = new List<Horse> { new Horse { Id = 1, Name = "Horse 1", Odds = 2 } } },
            });

            var sut = new RaceService(raceApi, betApi);
            var races = await sut.GetRaces();

            var race = races.Single(r => r.Id == 1);
            var horse = race.Horses.First();
            Assert.AreEqual(1, horse.Id);
            Assert.AreEqual("Horse 1", horse.Name);
            Assert.AreEqual(2, horse.Odds);
        }

        [Test]
        public void RacesController_ShouldReturnOk_WhenRacesAreRetrieved()
        {
            var service = Substitute.For<IRaceService>();
            service.GetRaces().Returns(Task.FromResult<IEnumerable<Models.Race>>(new List<Models.Race>() { new Models.Race()}));

            var raceController = new RacesController(service);
            var response = raceController.Get();

            Assert.IsInstanceOf<OkObjectResult>(response.Result);
        }

        [Test]
        public void RacesController_ShouldReturnOk_WhenThereAreNoRaces()
        {
            var service = Substitute.For<IRaceService>();
            service.GetRaces().Returns(Task.FromResult<IEnumerable<Models.Race>>(new List<Models.Race>()));

            var raceController = new RacesController(service);
            var response = raceController.Get();

            Assert.IsInstanceOf<OkObjectResult>(response.Result);
        }
    }
}
