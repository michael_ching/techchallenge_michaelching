import React, { Component } from 'react';
import RaceDetails from './RaceDetails';
import Moment from 'react-moment';

class Race extends Component {
    render() {
        const { races } = this.props;
        return (
            races.map((race) => {
                return (
                    <div>
                        <p><span>Race: {race.name}</span>&nbsp;
                        <span>Start time: <Moment>{race.startTime}</Moment></span>&nbsp;
                        <span>Bet amount: {race.amount}</span>&nbsp;
                        <span>Status: {race.status}</span></p>&nbsp;

                        <RaceDetails horses={race.horses} />
                    </div>
                );
            })
        );
    }
}

export default Race;