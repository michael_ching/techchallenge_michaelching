import React, { Component } from 'react';
import {Table} from 'react-bootstrap';

class RaceDetails extends Component {
  render() {
    const rows = [];

    this.props.horses.forEach((horse) => {
        rows.push(<HorseRow 
            name={horse.name} 
            odds={horse.odds}
            bets={horse.numOfBets}
            payout={horse.payout}
        />);
    } );

    return (
      <div>
        <Table striped bordered condensed hover responsive>
          <thead>
            <th>Horse</th>
            <th>Odds</th>
            <th>Bets</th>
            <th>Payout</th>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </Table>
      </div>
    )
  }
}

class HorseRow extends Component {
    render() {
        const odds = this.props.odds;
        const name = this.props.name;
        const bets = this.props.bets;
        const payout = this.props.payout;

        return (
            <tr>
                <td>{name}</td>
                <td>{odds}</td>
                <td>{bets}</td>
                <td>{payout}</td>
            </tr>
        );
    }
}

export default RaceDetails;