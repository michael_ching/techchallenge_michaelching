import React, { Component } from 'react';
import './App.css';
import Race from './Race';

class App extends Component {
  constructor() {
    super();
    this.state = {
      races: [],
      hasError: false,
      isLoading: false
    };
  }

  fetchData(url) {
    this.setState({isLoading: true});

    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        this.setState({isLoading: false});
        return response;
      })
      .then((response) => response.json())
      .then((races) => this.setState({ races }))
      .catch((error) => {
        this.setState({ hasError: true });
        console.log(error);
      });
  }

  componentDidMount() {
    this.fetchData('http://localhost:50606/api/races/');
  }

  render() {
    if (this.state.hasError) {
      return (
        <p> There was an error while retrieving data </p>
      );
    }

    if (this.state.isLoading) {
      return (
        <p> Loading... </p>  
      );
    }

    return (
      <div className="App">
        <Race races={this.state.races} />
      </div>
    );
  }
}

export default App;