# Race Day

## Solution Design
This solution consists of two components:

1. ASP.NET Core back end solution that integrates with the API endpoints provided

2. React application for the front end

### Back end solution
#### Endpoints
The application exposes the following endpoints:

* `GET /api/races`
This retrieves all the races

* `GET /api/customers`
This retrieves a list of all customers

* `GET /api/customers/{customerId}`
This retrieves the details for the specified customer, including the number of bets and the total amount

* `GET /api/customers/atRisk`
This retrieves a list of at risk customers

* `GET /api/totalBetAmount`
This retrieves the total bet amount across all customers

#### Consumed endpoints
* https://whatech-customerbets.azurewebsites.net/api/GetBetsV2?name=michaelChing

* https://whatech-customerbets.azurewebsites.net/api/GetCustomers?name=michaelChing

* https://whatech-customerbets.azurewebsites.net/api/GetRaces?name=michaelChing

## TODOs
1. Better error handling and logging
2. Better test coverage - acceptance tests and more unit tests
3. Performance - currently all bets and all races are retrieved on every request, which is not scalable. There should be caching of data at different layers to improve this.
4. Api versioning
5. Security

## Getting Started
### Prerequisites
The following is needed:

* npm

* .net core 2

* a browser

The following ports are needed and should not be used by other applications:

* 3000

* 50606

### Installing
Run Build.bat at the root folder. This would both build and run the back end and the front end solutions.